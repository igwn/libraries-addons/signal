// @(#)root/kfr:$Id$
// Author: Marco Meyer   30/12/2023

/*************************************************************************
 * Copyright (C) 1995-2023, Rene Brun and Fons Rademakers.               *
 * All rights reserved.                                                  *
 *                                                                       *
 * For the licensing terms see $ROOTSYS/LICENSE.                         *
 * For the list of contributors see $ROOTSYS/README/CREDITS.             *
 *************************************************************************/

////////////////////////////////////////////////////////////////////////////////
/// \class TKFRLegacy *ComplexReal
///
/// One of the interface classes to the KFR package, can be used directly
/// or via the TVirtualFFT class. Only the basic interface of KFR is implemented.
///
/// Computes the inverse of the real-to-complex transforms (class TKFRLegacy *RealComplex)
/// taking complex input (storing the non-redundant half of a logically Hermitian array)
/// to real output (see KFR manual for more details)
///
/// How to use it:
/// 1. Create an instance of TKFRLegacy *ComplexReal - this will allocate input and output
///    arrays (unless an in-place transform is specified)
/// 2. Run the Init() function with the desired flags and settings
/// 3. Set the data (via SetPoints(), SetPoint() or SetPointComplex() functions)
/// 4. Run the Transform() function
/// 5. Get the output (via GetPoints(), GetPoint() or GetPointReal() functions)
/// 6. Repeat steps 3)-5) as needed
///
/// For a transform of the same size, but with different flags, rerun the Init()
/// function and continue with steps 3)-5)
///
/// NOTE:
///       1. running Init() function will overwrite the input array! Don't set any data
///          before running the Init() function
///       2. KFR computes unnormalized transform, so doing a transform followed by
///          its inverse will lead to the original array scaled by the transform size
///       3. In Complex to Real transform the input array is destroyed. It cannot then
///          be retrieved when using the Get's methods.
///
////////////////////////////////////////////////////////////////////////////////

#include "TKFRLegacy *ComplexReal.h"
#include "TComplex.h"
#include "Riostream.h"

#include "kfr/dft/fft.hpp"
#include "kfr/version.hpp"
namespace kfr
{
const char* library_version_dft();
} // namespace kfr

ClassImp(TKFRLegacy *ComplexReal);
using complex = kfr::complex<double>;

////////////////////////////////////////////////////////////////////////////////
///default

TKFRLegacy *ComplexReal::TKFRLegacy *ComplexReal()
{
   fIn   = 0;
   fOut  = 0;
   fTemp = 0;
   fPlan = 0;
   fN    = 0;
   fTotalSize = 0;
   fNdim = 0;
}

////////////////////////////////////////////////////////////////////////////////
///For 1d transforms
///Allocates memory for the input array, and, if inPlace = kFALSE, for the output array

TKFRLegacy *ComplexReal::TKFRLegacy *ComplexReal(Int_t n, Bool_t inPlace)
{
   fIn = new std::vector<complex>(n/2+1, 0);
   fOut = 0;
   if (!inPlace)
      fOut = new std::vector<double>(n, 0);

   fTemp = 0;

   fN    = new Int_t[1];
   fN[0] = n;
   fTotalSize = n;
   fNdim = 1;
   fPlan = 0;
   fTemp = 0;
}

////////////////////////////////////////////////////////////////////////////////
///For ndim-dimensional transforms
///Second argument contains sizes of the transform in each dimension

TKFRLegacy *ComplexReal::TKFRLegacy *ComplexReal(Int_t ndim, Int_t *n, Bool_t inPlace)
{
   if (ndim > 1)
      throw std::invalid_argument("KFR5 is only implementing 1D FFT.");

   fNdim = ndim;
   fTotalSize = 1;
   fN = new Int_t[fNdim];
   for (Int_t i=0; i<fNdim; i++){
      fN[i] = n[i];
      fTotalSize*=n[i];
   }

   Int_t sizein = Int_t(Double_t(fTotalSize)*(n[ndim-1]/2+1)/n[ndim-1]);
   fIn = new std::vector<complex>(sizein, 0);
   fOut = 0;
   if (!inPlace)
      fOut = new std::vector<double>(fTotalSize, 0);

   fTemp = 0;
   fPlan = 0;
   fTemp = 0;
}


////////////////////////////////////////////////////////////////////////////////
///Destroys the data arrays and the plan. However, some plan information stays around
///until the root session is over, and is reused if other plans of the same size are
///created

TKFRLegacy *ComplexReal::~TKFRLegacy *ComplexReal()
{
   delete reinterpret_cast<kfr::dft_plan_real<double>*>(fPlan);
   fPlan = 0;

   delete ((std::vector<complex>*) fIn);
   fIn = 0;

   if(fOut) 
      delete ((std::vector<double>*) fOut);
   fOut = 0;
   if (fTemp)
      delete ((std::vector<kfr::u8>*) fTemp);
   fTemp = 0;

   if (fN)
      delete [] fN;
   fN = 0;
}

////////////////////////////////////////////////////////////////////////////////
///Creates the fftw-plan
///
///NOTE:  input and output arrays are overwritten during initialisation,
///       so don't set any points, before running this function!!!!!
///
///Arguments sign and kind are dummy and not need to be specified
///Possible flag_options:
///
/// - "ES" (from "estimate") - no time in preparing the transform, but probably sub-optimal
///   performance
/// - "M" (from "measure") - some time spend in finding the optimal way to do the transform
/// - "P" (from "patient") - more time spend in finding the optimal way to do the transform
/// - "EX" (from "exhaustive") - the most optimal way is found
///
///This option should be chosen depending on how many transforms of the same size and
///type are going to be done. Planning is only done once, for the first transform of this
///size and type.

void TKFRLegacy *ComplexReal::Init( Option_t *flags, Int_t /*sign*/,const Int_t* /*kind*/)
{
   fFlags = flags;

   if (fPlan) delete reinterpret_cast<kfr::dft_plan_real<double>*>(fPlan);
   fPlan = new kfr::dft_plan_real<double>(fTotalSize);
   if(fFlags.Contains("D")) reinterpret_cast<kfr::dft_plan_real<double>*>(fPlan)->dump();

   if (fTemp) delete ((std::vector<kfr::u8>*) fTemp);
   fTemp = new std::vector<kfr::u8>(reinterpret_cast<kfr::dft_plan_real<double>*>(fPlan)->temp_size, 0);
}

////////////////////////////////////////////////////////////////////////////////
///Computes the transform, specified in Init() function

void TKFRLegacy *ComplexReal::Transform()
{
   std::vector<complex>* _fIn   = reinterpret_cast<std::vector<complex>*>(fIn);
   std::vector<double>* _fOut   = reinterpret_cast<std::vector<double>*>(fOut ? fOut : fIn);
   std::vector<kfr::u8>* _fTemp = reinterpret_cast<std::vector<kfr::u8>*>(fTemp);

   if (fPlan) {
      reinterpret_cast<kfr::dft_plan_real<double>*>(fPlan)->execute(&(*_fOut)[0], &(*_fIn)[0], &(*_fTemp)[0]);
   } else {
      Error("Transform", "transform not initialised");
      return;
   }
}

////////////////////////////////////////////////////////////////////////////////
///Fills the argument array with the computed transform
/// Works only for output (input array is destroyed in a C2R transform)

void TKFRLegacy *ComplexReal::GetPoints(Double_t *data, Bool_t fromInput) const
{
   if (fromInput){
      Error("GetPoints", "Input array has been destroyed");
      return;
   }

   std::vector<complex>* _fIn   = reinterpret_cast<std::vector<complex>*>(fIn);
   std::vector<double>* _fOut   = reinterpret_cast<std::vector<double>*>(fOut ? fOut : fIn);
   std::copy(_fOut->begin(), _fOut->begin()+fTotalSize, data);
}

////////////////////////////////////////////////////////////////////////////////
///Returns the point #ipoint
/// Works only for output (input array is destroyed in a C2R transform)

Double_t TKFRLegacy *ComplexReal::GetPointReal(Int_t ipoint, Bool_t fromInput) const
{
   if (fromInput){
      Error("GetPointReal", "Input array has been destroyed");
      return 0;
   }
   
   std::vector<complex>* _fIn   = reinterpret_cast<std::vector<complex>*>(fIn);
   std::vector<double>* _fOut   = reinterpret_cast<std::vector<double>*>(fOut ? fOut : fIn);
   return (*_fOut)[ipoint];
}

////////////////////////////////////////////////////////////////////////////////
///For multidimensional transforms. Returns the point #ipoint
/// Works only for output (input array is destroyed in a C2R transform)

Double_t TKFRLegacy *ComplexReal::GetPointReal(const Int_t *ipoint, Bool_t fromInput) const
{
   if (fromInput){
      Error("GetPointReal", "Input array has been destroyed");
      return 0;
   }
   Int_t ireal = ipoint[0];
   for (Int_t i=0; i<fNdim-1; i++)
      ireal=fN[i+1]*ireal + ipoint[i+1];

   std::vector<complex>* _fIn   = reinterpret_cast<std::vector<complex>*>(fIn);
   std::vector<double>* _fOut   = reinterpret_cast<std::vector<double>*>(fOut ? fOut : fIn);
   return (*_fOut)[ireal];
}


////////////////////////////////////////////////////////////////////////////////
/// Works only for output (input array is destroyed in a C2R transform)

void TKFRLegacy *ComplexReal::GetPointComplex(Int_t ipoint, Double_t &re, Double_t &im, Bool_t fromInput) const
{
   if (fromInput){
      Error("GetPointComplex", "Input array has been destroyed");
      return;
   }

   std::vector<double>* _fOut   = reinterpret_cast<std::vector<double>*>(fOut ? fOut : fIn);
   re = (*_fOut)[ipoint];
   im = 0;
}

////////////////////////////////////////////////////////////////////////////////
///For multidimensional transforms. Returns the point #ipoint
/// Works only for output (input array is destroyed in a C2R transform)

void TKFRLegacy *ComplexReal::GetPointComplex(const Int_t *ipoint, Double_t &re, Double_t &im, Bool_t fromInput) const
{
   if (fromInput){
      Error("GetPointComplex", "Input array has been destroyed");
      return;
   }
   const Double_t * array =  (const Double_t*) ( (fOut) ?  fOut :  fIn );

   Int_t ireal = ipoint[0];
   for (Int_t i=0; i<fNdim-1; i++)
      ireal=fN[i+1]*ireal + ipoint[i+1];

   std::vector<double>* _fOut   = reinterpret_cast<std::vector<double>*>(fOut ? fOut : fIn);
   re = (*_fOut)[ireal];
   im = 0;
}
////////////////////////////////////////////////////////////////////////////////
///Returns the array of computed transform
/// Works only for output (input array is destroyed in a C2R transform)

Double_t* TKFRLegacy *ComplexReal::GetPointsReal(Bool_t fromInput) const
{
   // we have 2 different cases
   // fromInput = false; fOut = !NULL (transformed is not in place) : return fOut
   // fromInput = false; fOut = NULL (transformed is in place) : return fIn

   if (fromInput) {
      Error("GetPointsReal","Input array was destroyed");
      return 0;
   }
   
   std::vector<double>* _fOut   = reinterpret_cast<std::vector<double>*>(fOut ? fOut : fIn);
   return &(*_fOut)[0] ;
}

////////////////////////////////////////////////////////////////////////////////
///Fills the argument array with the computed transform
/// Works only for output (input array is destroyed in a C2R transform)

void TKFRLegacy *ComplexReal::GetPointsComplex(Double_t *re, Double_t *im, Bool_t fromInput) const
{
   if (fromInput){
      Error("GetPointsComplex", "Input array has been destroyed");
      return;
   }
   const Double_t * array =  (const Double_t*) ( (fOut) ?  fOut :  fIn );
   for (Int_t i=0; i<fTotalSize; i++){
      re[i] = array[i];
      im[i] = 0;
   }
}

////////////////////////////////////////////////////////////////////////////////
///Fills the argument array with the computed transform.
/// Works only for output (input array is destroyed in a C2R transform)

void TKFRLegacy *ComplexReal::GetPointsComplex(Double_t *data, Bool_t fromInput) const
{
   if (fromInput){
      Error("GetPointsComplex", "Input array has been destroyed");
      return;
   }
   const Double_t * array =  (const Double_t*) ( (fOut) ?  fOut :  fIn );
   for (Int_t i=0; i<fTotalSize; i+=2){
      data[i] = array[i/2];
      data[i+1]=0;
   }
}

////////////////////////////////////////////////////////////////////////////////
///since the input must be complex-Hermitian, if the ipoint > n/2, the according
///point before n/2 is set to (re, -im)

void TKFRLegacy *ComplexReal::SetPoint(Int_t ipoint, Double_t re, Double_t im)
{
   std::vector<complex>* _fIn = reinterpret_cast<std::vector<complex>*>(fIn);
   std::vector<double>* _fOut = reinterpret_cast<std::vector<double>*>(fOut ? fOut : fIn);
   if (ipoint <= fN[0]/2){
      (*_fIn)[ipoint] = complex(re, im);
   } else {
      (*_fOut)[2*(fN[0]/2)-ipoint] = re;
   }
}

////////////////////////////////////////////////////////////////////////////////
///Set the point #ipoint. Since the input is Hermitian, only the first (roughly)half of
///the points have to be set.

void TKFRLegacy *ComplexReal::SetPoint(const Int_t *ipoint, Double_t re, Double_t im)
{
   Int_t ireal = ipoint[0];
   for (Int_t i=0; i<fNdim-2; i++){
      ireal=fN[i+1]*ireal + ipoint[i+1];
   }
   ireal = (fN[fNdim-1]/2+1)*ireal+ipoint[fNdim-1];
   Int_t realN = Int_t(Double_t(fTotalSize)*(fN[fNdim-1]/2+1)/fN[fNdim-1]);

   if (ireal > realN){
      Error("SetPoint", "Illegal index value");
      return;
   }

   std::vector<complex>* _fIn = reinterpret_cast<std::vector<complex>*>(fIn);
   (*_fIn)[ireal] = complex(re, im);
}

////////////////////////////////////////////////////////////////////////////////
///since the input must be complex-Hermitian, if the ipoint > n/2, the according
///point before n/2 is set to (re, -im)

void TKFRLegacy *ComplexReal::SetPointComplex(Int_t ipoint, TComplex &c)
{
   std::vector<complex>* _fIn = reinterpret_cast<std::vector<complex>*>(fIn);
   if (ipoint <= fN[0]/2){
      (*_fIn)[ipoint] = complex(c.Re(), c.Im());
   } else {
      (*_fIn)[2*(fN[0]/2)-ipoint] = complex(c.Re(), -c.Im());
   }
}

////////////////////////////////////////////////////////////////////////////////
///set all points. the values are copied. points should be ordered as follows:
///[re_0, im_0, re_1, im_1, ..., re_n, im_n)

void TKFRLegacy *ComplexReal::SetPoints(const Double_t *data)
{
   std::vector<complex>* _fIn = reinterpret_cast<std::vector<complex>*>(fIn);
   Int_t sizein = Int_t(Double_t(fTotalSize)*(fN[fNdim-1]/2+1)/fN[fNdim-1]);
   for (Int_t i=0; i<2*(sizein); i+=2){
      (*_fIn)[i/2] = complex(data[i], data[i+1]);
   }
}

////////////////////////////////////////////////////////////////////////////////
///Set all points. The values are copied.

void TKFRLegacy *ComplexReal::SetPointsComplex(const Double_t *re, const Double_t *im)
{
   std::vector<complex>* _fIn = reinterpret_cast<std::vector<complex>*>(fIn);
   Int_t sizein = Int_t(Double_t(fTotalSize)*(fN[fNdim-1]/2+1)/fN[fNdim-1]);
   for (Int_t i=0; i<sizein; i++){
      (*_fIn)[i] = complex(re[i], im[i]);
   }
}

////////////////////////////////////////////////////////////////////////////////
///allowed options:
///"ES" - FFTW_ESTIMATE
///"M" - FFTW_MEASURE
///"P" - FFTW_PATIENT
///"EX" - FFTW_EXHAUSTIVE

UInt_t TKFRLegacy *ComplexReal::MapFlag(Option_t *flag)
{
   throw std::invalid_argument("Not implemented in KFR");

   // TString opt = flag;
   // opt.ToUpper();
   // if (opt.Contains("ES"))
   //    return FFTW_ESTIMATE;
   // if (opt.Contains("M"))
   //    return FFTW_MEASURE;
   // if (opt.Contains("P"))
   //    return FFTW_PATIENT;
   // if (opt.Contains("EX"))
   //    return FFTW_EXHAUSTIVE;
   // return FFTW_ESTIMATE;
}

TString TKFRLegacy *ComplexReal::Version()
{
   return TString(kfr::library_version_dft());
}
