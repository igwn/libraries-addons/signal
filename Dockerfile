# Use the rootproject/root image as the base image
FROM rootproject/root:latest

# Update package lists and install necessary packages
ENV LANG=C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
ENV NPROC=8

ENV CXX="/usr/bin/clang++"
ENV CC="/usr/bin/clang"

ENV LD_LIBRARY_PATH=/usr/local/lib

# Update package lists and install necessary packages
COPY Dockerfile.packages packages

RUN apt-get update -qq && \
    ln -sf /usr/share/zoneinfo/UTC /etc/localtime && \
    apt-get -y install $(cat packages) wget git && \
    apt-get autoremove -y && \
    apt-get clean -y && \
    rm -rf /var/cache/apt/archives/* && \
    rm -rf /var/lib/apt/lists/*

# Install ROOT+ library
RUN git clone --recursive https://git.ligo.org/kagra/libraries-addons/root/root-plus.git /opt/root-plus
RUN /opt/root-plus/cmake/download_libtorch 2.3.1
ENV PATH="/opt/libtorch/bin:${PATH}"
ENV LD_LIBRARY_PATH="/opt/libtorch/lib:${LD_LIBRARY_PATH}"

RUN cd /opt/root-plus && mkdir build && cd build && cmake ..
RUN cd /opt/root-plus/build && make -j${NPROC} && make -j${NPROC} install
RUN rm -rf /opt/root-plus

# Download and install KFR library
RUN git clone https://gitlab.cern.ch/igwn/software/kfr.git /opt/kfr
RUN mkdir -p kfr/build
RUN cd kfr/build && cmake .. -DKFR_ENABLE_CAPI_BUILD=ON -DCMAKE_POSITION_INDEPENDENT_CODE=ON
RUN cd kfr/build && make -j${NPROC} && make -j${NPROC} install
RUN rm -rf kfr