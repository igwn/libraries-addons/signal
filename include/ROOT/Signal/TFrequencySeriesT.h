#pragma once

#include <Riostream.h>
#include <vector>
#include <complex>
#include "TH1D.h"

namespace ROOT {
    namespace Signal {

        template <typename T>
        class TFrequencySeriesT: public TTensorC {
            
            public:
                TFrequencySeriesT();
                TTimeSeriesT(Double_t deltaTime, Double_t epoch, const std::vector<Element>& values, const char* unit = "", const char* label = "");

                virtual ~TFrequencySeriesT();

                void Resize(UInt_t newSize);
                
            ClassDef(TFrequencySeriesT, 1);
        };
    }
}
