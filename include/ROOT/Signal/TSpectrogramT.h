#pragma once

#include <Riostream.h>
#include "TFrequencySeriesT.h"
#include "TH2.h"

namespace ROOT {
    namespace Signal {

        template <typename Element, typename = 
                  typename std::enable_if<std::is_same<Element, float>::value || std::is_same<Element, double>::value || std::is_same<Element, int>::value>::type>
        class TSpectrogramT: public TTimeFrequencyMap<Element> {

            public:
                TSpectrogramT();
                ~TSpectrogramT();

                TH2* GetHistogram(const char* name, const char* title) const;
                TFrequencySeriesT<T> &GetSlice(UInt_t i);

            ClassDef(TSpectrogramT, 1);
        };
    }
}
