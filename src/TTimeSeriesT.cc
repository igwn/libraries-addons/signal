#include "ROOT/Signal/TTimeSeriesT.h"

ClassImp(ROOT::Signal::TTimeSeriesT<double>)
ClassImp(ROOT::Signal::TTimeSeriesT<float>)

namespace ROOT {
    namespace Signal {

        template <typename T>
        TTimeSeriesT<T>::TTimeSeriesT()
            : fDeltaTime(1.0), tEpoch(0.0), f0(0.0), unitY(""), labelY("") {
            // Default constructor initializes delta time to 1 second, epoch to 0, f0 to 0, and unitY and labelY to an empty string
        }

        template <typename T>
        TTimeSeriesT<T>::TTimeSeriesT(Double_t deltaTime, Double_t epoch, const std::vector<NumberT>& values, const char* unit, const char* label)
            : fDeltaTime(deltaTime), tEpoch(epoch), fValues(values), f0(0.0), unitY(unit), labelY(label) {
            // Constructor with specified delta time, epoch, values, f0, unitY, and labelY
        }

        template <typename T>
        TTimeSeriesT<T>::~TTimeSeriesT() {
            // Destructor
        }

        template <typename T>
        Double_t TTimeSeriesT<T>::GetDeltaTime() const {
            return fDeltaTime;
        }

        template <typename T>
        Double_t TTimeSeriesT<T>::GetEpoch() const {
            return tEpoch;
        }

        template <typename T>
        Double_t TTimeSeriesT<T>::GetHeterodyneFrequency() const {
            return f0;
        }

        template <typename T>
        UInt_t TTimeSeriesT<T>::GetN() const {
            return fValues.size();
        }

        template <typename T>
        Double_t TTimeSeriesT<T>::GetLength() const {
            return fDeltaTime * fValues.size();
        }

        template <typename T>
        const char* TTimeSeriesT<T>::GetUnitY() const {
            return unitY;
        }

        template <typename T>
        const char* TTimeSeriesT<T>::GetLabelY() const {
            return labelY;
        }

        template <typename T>
        void TTimeSeriesT<T>::Add(int index, const NumberT& value) {
            if (index >= 0 && static_cast<size_t>(index) <= fValues.size()) {
                fValues.insert(fValues.begin() + index, value);
            }
        }

        template <typename T>
        void TTimeSeriesT<T>::Remove(int index) {
            if (index >= 0 && static_cast<size_t>(index) < fValues.size()) {
                fValues.erase(fValues.begin() + index);
            }
        }

        template <typename T>
        TH1* TTimeSeriesT<T>::GetHistogram(ComplexPart part, const char* name, const char* title) const {
            
            int nBins = this->GetN();
            TH1D* hist = new TH1D(name, title, nBins, 0, nBins);

            for (int i = 0; i < nBins; ++i) {

                if (std::is_same<decltype(fValues[i]), T>::value)
                    hist->SetBinContent(i + 1, gKFRLegacy->Get(part, std::get<T>(fValues[i])));
                if (std::is_same<decltype(fValues[i]), std::complex<T>>::value)
                    hist->SetBinContent(i + 1, gKFRLegacy->Get(part, std::get<std::complex<T>>(fValues[i])));
            }

            return hist;
        }

        template <typename T>
        void TTimeSeriesT<T>::Clear() {
            fValues.clear();
        }

        template <typename T>
        typename TTimeSeriesT<T>::NumberT TTimeSeriesT<T>::Get(UInt_t index) const {
            if (index < this->GetN()) {
                return fValues[index];
            }

            // Return default value if index is out of range
            return 0.0;
        }

        template <typename T>
        T TTimeSeriesT<T>::GetReal(UInt_t index) const {
            if (index < this->GetN()) {

                if (std::is_same<decltype(fValues[index]), T>::value)
                    return std::get<T>(fValues[index]);
                if (std::is_same<decltype(fValues[index]), std::complex<T>>::value)
                    return gKFRLegacy->Real(std::get<std::complex<T>>(fValues[index]));
            }

            // Return default value if index is out of range
            return 0.0;
        }

        template <typename T>
        T TTimeSeriesT<T>::GetImag(UInt_t index) const {
            if (index < this->GetN()) {
                if (std::is_same<decltype(fValues[index]), T>::value)
                    return 0.0;
                if (std::is_same<decltype(fValues[index]), std::complex<T>>::value)
                    return gKFRLegacy->Imag(std::get<std::complex<T>>(fValues[index]));
            }

            // Return default value if index is out of range
            return 0.0;
        }

        template <typename T>
        void TTimeSeriesT<T>::Resize(UInt_t newSize) {
            fValues.resize(newSize);
        }

        template <typename T>
        bool TTimeSeriesT<T>::IsComplex() {
            
            for (size_t i = 0; i < fValues.size(); ++i) {
                
                if(std::holds_alternative<std::complex<T>>(fValues[i])) {
                    return true;
                }
            }

            return false;
        }

        template <typename T>
        TTimeSeriesT<T> TTimeSeriesT<T>::operator+(const TTimeSeriesT<T>& other) const {
            if (fValues.size() != other.fValues.size()) {
                std::cerr << "Error: Cannot add TTimeSeriesT objects with different sizes." << std::endl;
                return *this;
            }

            TTimeSeriesT<T> result(fDeltaTime, tEpoch, fValues, unitY, labelY);
            result.fValues.resize(fValues.size());

            for (size_t i = 0; i < fValues.size(); ++i) {

                if (std::is_same<decltype(fValues[i]), T>::value && std::is_same<decltype(other.fValues[i]), T>::value)
                    result.fValues[i] = std::get<T>(fValues[i]) + std::get<T>(other.fValues[i]);
                if (std::is_same<decltype(fValues[i]), std::complex<T>>::value && std::is_same<decltype(other.fValues[i]), std::complex<T>>::value)
                    result.fValues[i] = std::get<std::complex<T>>(fValues[i]) + std::get<std::complex<T>>(other.fValues[i]);
                if (std::is_same<decltype(fValues[i]), T>::value && std::is_same<decltype(other.fValues[i]), std::complex<T>>::value)
                    result.fValues[i] = static_cast<std::complex<T>>(std::get<T>(fValues[i])) + std::get<std::complex<T>>(other.fValues[i]);
                if (std::is_same<decltype(fValues[i]), std::complex<T>>::value && std::is_same<decltype(other.fValues[i]), T>::value)
                    result.fValues[i] = std::get<std::complex<T>>(fValues[i]) + static_cast<std::complex<T>>(std::get<T>(other.fValues[i]));
            }

            return result;
        }

        template <typename T>
        TTimeSeriesT<T> TTimeSeriesT<T>::operator-(const TTimeSeriesT<T>& other) const {
            if (fValues.size() != other.fValues.size()) {
                std::cerr << "Error: Cannot subtract TTimeSeriesT objects with different sizes." << std::endl;
                return *this;
            }

            TTimeSeriesT<T> result(fDeltaTime, tEpoch, fValues, unitY, labelY);
            result.fValues.resize(fValues.size());

            for (size_t i = 0; i < fValues.size(); ++i) {

                if (std::is_same<decltype(fValues[i]), T>::value && std::is_same<decltype(other.fValues[i]), T>::value)
                    result.fValues[i] = std::get<T>(fValues[i]) - std::get<T>(other.fValues[i]);
                if (std::is_same<decltype(fValues[i]), std::complex<T>>::value && std::is_same<decltype(other.fValues[i]), std::complex<T>>::value)
                    result.fValues[i] = std::get<std::complex<T>>(fValues[i]) - std::get<std::complex<T>>(other.fValues[i]);
                if (std::is_same<decltype(fValues[i]), T>::value && std::is_same<decltype(other.fValues[i]), std::complex<T>>::value)
                    result.fValues[i] = static_cast<std::complex<T>>(std::get<T>(fValues[i])) - std::get<std::complex<T>>(other.fValues[i]);
                if (std::is_same<decltype(fValues[i]), std::complex<T>>::value && std::is_same<decltype(other.fValues[i]), T>::value)
                    result.fValues[i] = std::get<std::complex<T>>(fValues[i]) - static_cast<std::complex<T>>(std::get<T>(other.fValues[i]));
            }


            return result;
        }

        template <typename T>
        typename TTimeSeriesT<T>::NumberT& TTimeSeriesT<T>::operator[](size_t index) {
            
            if (index < fValues.size()) {
                return fValues[index];
            }
            // Return default value if index is out of range
            static TTimeSeriesT<T>::NumberT default_value = 0.0;
            return default_value;
        }

        template <typename T>
        void TTimeSeriesT<T>::Print(Option_t* option) const {
            std::cout << "TTimeSeriesT: DeltaTime = " << fDeltaTime << ", Epoch = " << tEpoch << ", UnitY = " << unitY << ", LabelY = " << labelY << std::endl;
        }
    }
}

template class ROOT::Signal::TTimeSeriesT<float>;
template class ROOT::Signal::TTimeSeriesT<double>;