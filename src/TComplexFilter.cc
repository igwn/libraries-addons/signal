/**
 **********************************************
 *
 * \file TComplexFilter.cc
 * \brief Source code of the TComplexFilter class
 * \author Marco Meyer \<marco.meyer@cern.ch\>
 *
 *********************************************
 */

#include "ROOT/Signal/DSP/TComplexFilter.h"
ClassImp(ROOT::Signal::TComplexFilter)

#include "TKFRLegacy.h"
#include <TSpectrum.h>
#include <TPolyMarker.h>
#include <TArrow.h>

#include <TFitResultPtr.h>
#include <TFitResult.h>

namespace ROOT { namespace Signal {

    TComplexFilter::TComplexFilter(Filter type, double k, std::vector<double> B, std::vector<double> A): type(type)
    {
        int order = TMath::Max(B.size(), A.size());
        
        if(IsDigital()) std::reverse(B.begin(), B.end()); // Rearrange coefficients orders such that it matches polynomial (n>0)
        if(IsDigital()) std::reverse(A.begin(), A.end());
        
        B.resize(order, 0);
        for(int i = 0; i < order; i++)
            B[i] *= k;

        this->B = new ROOT::Math::Polynomial(order-1);
        this->B->SetParameters(&B[0]);

        A.resize(order, 0);
        this->A = new ROOT::Math::Polynomial(order-1);
        this->A->SetParameters(&A[0]);
    }

    void TComplexFilter::SetGain(double k)
    {
        std::vector<double> B = std::vector<double>(this->B->Parameters(), this->B->Parameters() + this->B->NPar());

        double k0 = B[0];
        for(int i = 0, N = this->B->NPar(); i < N; i++) {
            B[i] *= k/k0;
        }

        this->B->SetParameters(&B[0]);
    }

    TComplexFilter *TComplexFilter::Cast(Filter type, double Ts, FilterTransform method, double alpha)
    {
            if(IsDigital()) {

                    if(type == Filter::Digital) return this;

                    TF tf = gKFRLegacy->Analog(this->TransferFunction(), Ts, method, alpha);
                    return new TComplexFilter(Filter::Analog, tf.B, tf.A);

            } else {

                    if(type == Filter::Analog) return this;

                    TF tf = gKFRLegacy->Digital(this->TransferFunction(), Ts, method, alpha);
                    return new TComplexFilter(Filter::Digital, tf.B, tf.A);
            }
    }

    std::vector<double> TComplexFilter::Response(std::vector<double> x)
    {
        return gKFRLegacy->Filtering(x, this->SecondOrderSections());
    }

    std::vector<std::complex<double>> TComplexFilter::Response(std::vector<std::complex<double>> x)
    {
        ZPK zpk;
            zpk.zeros = this->GetZeros();
            zpk.poles = this->GetPoles();
            zpk.gain = this->GetGain();

        TF H = gKFRLegacy->TransferFunction(zpk);

        std::vector<std::complex<double>> y(x.size(), 0);
        for(int i = 0, N = x.size(); i < N; i++)
            y[i] = gKFRLegacy->Eval(x[i], H);

        return y;
    }

    void TComplexFilter::Print(Option_t *opt) const
    {
        TString x = this->IsDigital() ? "z" : "s";
        std::cout << "H(" << x << ") = B(" << x << ") / A(" << x << ")" << std::endl;
        
        std::vector<double> b = std::vector<double>(B->Parameters(), B->Parameters()+B->NPar());
        std::vector<double> a = std::vector<double>(A->Parameters(), A->Parameters()+A->NPar());

        std::cout << "B(" << x << ") = ";
        for(int i = 0, i0 = -1, N = b.size(), first = true; i < N; i++) {
            
            if( ROOT::IOPlus::Helpers::EpsilonEqualTo(b[i],0.0)) continue;
            if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(b[i],1.0)) std::cout << b[i];

            if(i0 < 0) i0 = i;
            if((i-i0) > 1) std::cout << x << "^" << (i-i0);
            else if ((i-i0) > 0) std::cout << x;

            if((!ROOT::IOPlus::Helpers::EpsilonEqualTo(b[i],0.0) && !ROOT::IOPlus::Helpers::EpsilonEqualTo(b[i],1.0)) && i < N-1) std::cout << " + ";
            else std::cout << " ";
            first = false;
        }
        std::cout << std::endl;

        std::cout << "A(" << x << ") = ";
        for(int i = 0, i0 = -1, N = a.size(); i < N; i++) {

            if( ROOT::IOPlus::Helpers::EpsilonEqualTo(a[i],0.0)) continue;
            if(!ROOT::IOPlus::Helpers::EpsilonEqualTo(a[i],1.0)) std::cout << a[i];
            
            if(i0 < 0) i0 = i;
            if((i-i0) > 1) std::cout << x << "^" << (i-i0);
            else if ((i-i0) > 0) std::cout << x;

            if((!ROOT::IOPlus::Helpers::EpsilonEqualTo(a[i],0.0) && !ROOT::IOPlus::Helpers::EpsilonEqualTo(a[i],1.0)) && i < N-1) std::cout << " + ";
        }
        std::cout << std::endl;
    }
}}
